# Specify phone tech before including full_phone
$(call inherit-product, vendor/aoip/configs/gsm.mk)

# Inherit some common CM stuff.
$(call inherit-product, vendor/aoip/configs/common.mk)

# Inherit device configuration
$(call inherit-product, $ device/samsung/ancora_tmo/full_ancora_tmo.mk)

PRODUCT_RELEASE_NAME := ancora_tmo

# Setup device configuration
PRODUCT_NAME := ancora_tmo
PRODUCT_DEVICE := ancora_tmo
PRODUCT_BRAND := samsung
PRODUCT_MANUFACTURER := samsung
PRODUCT_MODEL := SGH-T679

PRODUCT_BUILD_PROP_OVERRIDES += BUILD_FINGERPRINT=samsung/SGH-T679/SGH-T679:2.3.6/GINGERBREAD/UVLE1:user/release-keys PRIVATE_BUILD_DESC="SGH-T679-user 2.3.6 GINGERBREAD UVLE1 release-keys"

# Copy device specific prebuilt files.
PRODUCT_COPY_FILES += \
    vendor/aoip/prebuilt/bootanimations/BOOTANIMATION-480x800.zip:system/media/bootanimation.zip
